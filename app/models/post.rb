class Post < ActiveRecord::Base
  attr_accessible :answers, :content, :rating, :title, :visible
end
